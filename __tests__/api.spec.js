const api = require('../index');

describe('api', () => {
  test('Library exists', () => {
    expect(api).toBeTruthy();
  });

  describe('get function', () => {
    test('returns an object', done => {
      const url = 'http://httpbin.org/get';
      const p = api.doGet(url);
      expect(typeof p).toEqual('object');
      p.then(response => {
        expect(response.status).toBe(200);
        expect(response.data).toBeDefined();
        expect(response.data.url).toBeDefined();
        expect(response.data.url).toBe('http://httpbin.org/get');
        done();
      }).catch(err => {
        expect(false).toBe(true);
        done();
      });
    });
  });
});
