const axios = require('axios');

const doGet = (url, options) => {
  return axios.get(url, options);
};

module.exports = {
  doGet,
};
